# TEAM MAVEN #

Our presentation highlights various aspects of the ecosystem behind Maven, Maven Central, Gradle, and Continuous Integration.

### Relevant Files ###

* **`TeamMaven/TeamMaven.pptx`** : slide deck for our presentation

* **`TeamMaven/src/...`** : a simple class with a JUnit test so that Maven/Bamboo has a test to run

* **`TeamMaven/mavenCentral/ex_pom.xml`** : example `pom.xml` file with necessary metadata for publishing a given artifact onto SonaType Nexus/Maven Central repositories
* **`TeamMaven/pom.xml`** : pom file with some new tags. Also is the pom file for the simple java program used in bamboo demonstration
* **`TeamMaven/src/main/java/MainApp.java`** : Simple java class for bamboo demonstration
* **`TeamMaven/src/test/java/MainAppTest.java`** : Simple JUnit test so that maven and bamboo have a unit test to run

* **`TeamMaven/GradlePresentation/`** : a simple Hello World Java application built with Gradle

### Team Members ###

* Terry Tao

* Anuved Verma

* Sherry Wang