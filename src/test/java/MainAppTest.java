import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by ttao on 8/11/2016.
 */
public class MainAppTest {
    @Test
    public void testGetDayType_shouldBeOneOfAllowedValues() {
        Set<String> allowedDayTypes = new HashSet<String>();
        allowedDayTypes.add("AWESOME");
        allowedDayTypes.add("AMAZING");

        String dayType = MainApp.getDayType();

        Assert.assertTrue(dayType != null);
        Assert.assertTrue(allowedDayTypes.contains(dayType));
    }
}
