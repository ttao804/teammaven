import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Created by ttao on 8/11/2016.
 */
public class MainApp {
    public static void main(String[] args) throws IOException {
        System.out.println("Hello World");

        System.out.println("Today is a " + getDayType() + " day");

        Date now = new Date();
        List<String> lines = Arrays.asList("Deployed "+now.toString());
        Files.write(Paths.get("deployment.log"), lines);
    }

    protected static String getDayType() {
        Random random = new Random();
        switch (random.nextInt(2)) {
            case 0:
                return "AMAZING";
            case 1:
                return "AWESOME";
        }

        throw new IllegalArgumentException("Clearly, my understanding of Random and switch is not accurate.");
    }
}
